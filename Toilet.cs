using Godot;
using System;

public class Toilet : MeshInstance
{
    private bool areaEntered = false;
	private bool canOpen = false;
    MeshInstance toiletCan;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        toiletCan = GetNode<MeshInstance>("ToiletModern001B");
        
    }

    public override void _Process(float delta)
	{
		if (Input.IsActionJustPressed("UniversalF"))
		{
			if (areaEntered)
			{
				if (!canOpen)
				{
                    toiletCan.RotateX(Mathf.Deg2Rad(-90));
					canOpen = true;
				}
				else if (canOpen)
				{
					toiletCan.RotateX(Mathf.Deg2Rad(90));
					canOpen = false;
				}
			}
		}
	}

	void onBodyEntered(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			GD.Print("Entered");
			GD.Print("Press F to open");
			areaEntered = true;

		}

	}

	void onBodyExited(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			GD.Print("Left the prox");
			areaEntered = false;
		}
	}
}
