using Godot;
using System;

public class Player : KinematicBody
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	[Export] float speed = 5;
	Vector3 direction = Vector3.Zero;
	Vector3 velocity = Vector3.Zero;
	[Export] float Gravity = 9.8f;
	[Export] float friction = 2f;
	[Export] float jumpSpeed = 5f;
	private Camera camera;

	[Export] float groundAccelerate = 5f;
	[Export] float maxVelocityGround = 10f;
	[Export] float airAccelerate = 20f;
	[Export] float maxVelocityAir = 40f;


	//Mouse
	[Export] float mouseSensitivity = 0.05f;


	RayCast groundRayCast;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		groundRayCast = GetNode<RayCast>("GroundRayCast");
		camera = GetNode<Camera>("Camera");
		Input.SetMouseMode(Input.MouseMode.Captured);
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		if (Input.IsActionJustPressed("ui_cancel"))
		{
			if (Input.GetMouseMode() == Input.MouseMode.Visible)
			{
				Input.SetMouseMode(Input.MouseMode.Captured);
			}
			else
			{
				Input.SetMouseMode(Input.MouseMode.Visible);
			}
		}
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		float vertical = Input.GetActionStrength("MoveBackward") - Input.GetActionStrength("MoveForward");
		float horizontal = Input.GetActionStrength("MoveRight") - Input.GetActionStrength("MoveLeft");

		Vector3 inputDir = new Vector3(horizontal, 0, vertical).Normalized();
		Vector3 wishDir = Vector3.Zero;
		wishDir += -GlobalTransform.basis.z * -inputDir.z;
		wishDir += GlobalTransform.basis.x * inputDir.x;

		if (Input.IsActionJustReleased("Jump") && IsOnFloor())
		{
			velocity.y = jumpSpeed;
		}

		if (IsOnFloor())
		{
			velocity = MoveGround(wishDir, velocity, delta);
		}
		else
		{
			velocity = MoveAir(wishDir, velocity, delta);
		}

		//velocity += direction * speed * delta;
		velocity.y -= Gravity * delta;

		velocity = MoveAndSlide(velocity, Vector3.Up);

		//velocity -= velocity * friction;


	}

	public override void _Input(InputEvent @event)
	{
		if (@event is InputEventMouseMotion && Input.GetMouseMode() == Input.MouseMode.Captured)
		{
			//Look up / down
			InputEventMouseMotion mouseEvent = (InputEventMouseMotion)@event;
			camera.RotateX(Mathf.Deg2Rad(-mouseEvent.Relative.y * mouseSensitivity));
			camera.RotationDegrees = new Vector3(Mathf.Clamp(camera.RotationDegrees.x, -90f, 90f), camera.RotationDegrees.y, camera.RotationDegrees.z);

			//Look left / right
			RotateY(Mathf.Deg2Rad(-mouseEvent.Relative.x) * mouseSensitivity);
		}
	}

	private Vector3 Accelerate(Vector3 wishDir, Vector3 prevVelocity, float accelerate, float maxVelocity, float delta)
	{
		wishDir = wishDir.Normalized();

		float projVel = prevVelocity.Dot(wishDir);
		float accelVel = accelerate * delta;

		if (projVel + accelVel > maxVelocity)
		{
			accelVel = maxVelocity - projVel;
		}

		return prevVelocity + wishDir * accelVel;
	}

	private Vector3 MoveGround(Vector3 wishDir, Vector3 prevVelocity, float delta)
	{
		float speed = prevVelocity.Length();
		if (speed != 0)
		{
			float drop = speed * friction * delta;
			prevVelocity *= Mathf.Max(speed - drop, 0) / speed;
		}

		return Accelerate(wishDir, prevVelocity, groundAccelerate, maxVelocityGround, delta);
	}

	private Vector3 MoveAir(Vector3 wishDir, Vector3 prevVelocity, float delta)
	{
		return Accelerate(wishDir, prevVelocity, airAccelerate, maxVelocityAir, delta);
	}
}
