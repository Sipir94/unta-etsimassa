using Godot;
using System;

public class Television : MeshInstance
{
	VideoStream stream = (VideoStream)ResourceLoader.Load("res://output.ogv");

	VideoPlayer player;

	private bool areaEntered = false;
	private bool tvOn = false;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		player = new VideoPlayer();
		player.Stream = stream;
		AddChild(player);
		Texture texture = player.GetVideoTexture();
		SpatialMaterial material = new SpatialMaterial();
		//material.DetailNormal = texture;
		material.AlbedoTexture = texture;
		//material.SetFlag(SpatialMaterial.Flags.Unshaded, true);
		material.FlagsUnshaded = true;
		//material.Uv2Triplanar = ;
		this.MaterialOverride = material;
		//GetNode<MeshInstance>("TV/Screen").MaterialOverride = material;
        player.Volume = 0.2f;
		
	}

	public override void _Process(float delta)
	{
		if (Input.IsActionJustPressed("UniversalF"))
		{
			if (areaEntered)
			{
				if (!tvOn)
				{
					player.Play();
					tvOn = true;
				}
				else if (tvOn)
				{
					player.Stop();
					tvOn = false;
				}
			}
		}
	}

	void onBodyEntered(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			GD.Print("Entered");
			GD.Print("Press F to open");
			areaEntered = true;

		}

	}

	void onBodyExited(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			GD.Print("Left the prox");
			areaEntered = false;
		}
	}
}
