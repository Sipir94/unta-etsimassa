extends Spatial

class_name GameTime

signal lose

# Declare member variables here. Examples:
export (NodePath) var hours_needle_path
onready var hours_needle = get_node(hours_needle_path)

export (NodePath) var minute_needle_path
onready var minute_needle = get_node(minute_needle_path)

export (NodePath) var seconds_needle_path
onready var seconds_needle = get_node(seconds_needle_path)

export (int) var timeX
export (bool) var forceGameTime
var realtime = false

var c_h = 0
var c_m = 0
var c_s = 0
var c_time = 0
var radian_seconds = null
var radian_minutes = null
var radian_hours = null

# Called when the node enters the scene tree for the first time.
func _ready():
	c_h = 22
	c_m = 00
	c_s = 00
	var h = OS.get_time()["hour"]
	if(h > 22 || h < 6):
		realtime = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#var h = OS.get_time()["hour"]
	#var m = OS.get_time()["minute"]
	#var s = OS.get_time()["second"]
	if(realtime == true && forceGameTime == false):
		radian_seconds = OS.get_time()["second"] * (PI/30)
		radian_minutes = OS.get_time()["minute"] * (PI/30)
		radian_hours = OS.get_time()["hour"] * (PI/6)
		c_h = OS.get_time()["hour"]
		c_m == OS.get_time()["minute"]
	else :
		c_s += delta * timeX
		if(c_s > 60):
			c_s = 1
			c_m += 1
		if(c_m > 60):
			c_m = 1
			c_h += 1
		if(c_h > 24):
			c_h = 1
			c_m = 0
			c_s = 0
		radian_seconds = c_s * (PI/30)
		radian_minutes = c_m * (PI/30)
		radian_hours = c_h* (PI/6)
	
	if(c_h == 5 && c_m == 1):
		emit_signal("lose")
	
	
	#radian_seconds = OS.get_time()["second"] * (PI/30)
	#radian_minutes = OS.get_time()["minute"] * (PI/30)
	#radian_hours = OS.get_time()["hour"] * (PI/6)
	
	#print("H ", h, " M ", m , " S ", s)
	
	var rot = hours_needle.get("rotation")
	rot.z = -radian_hours
	hours_needle.set("rotation", rot)
	
	rot = minute_needle.get("rotation")
	rot.z = -radian_minutes
	minute_needle.set("rotation", rot)
	
	rot = seconds_needle.get("rotation")
	rot.z = -radian_seconds
	seconds_needle.set("rotation", rot)
	
func set_time(inputHour, inputMin):
	c_h = inputHour
	c_m = inputMin
