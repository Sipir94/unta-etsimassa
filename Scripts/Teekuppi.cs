using Godot;
using System;

public class Teekuppi : KinematicBody
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	bool areaEntered = false;

	bool isPrepared = false;
	public bool isReady = false;
	bool isDrinked = true;

	MeshInstance teepussi;
	Tween tweenMovement;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		teepussi = GetNode<MeshInstance>("Teepussi");
		tweenMovement = GetNode<Tween>("Tweener");
	}

	// Called every frame. 'delta' is the elapsed time since the previous frame.
	public override void _Process(float delta)
	{
		if (Input.IsActionJustPressed("UniversalF"))
		{
			if (areaEntered)
			{
				if (!isPrepared)
				{
					tweenMovement.InterpolateProperty(teepussi,"translation",
					teepussi.Transform.origin,GetNode<Spatial>("PrepPos").Transform.origin,1,
					Tween.TransitionType.Linear,Tween.EaseType.InOut,0);
					tweenMovement.Start();
					isPrepared = true;
				}
				else if (isReady)
				{
					isReady = false;
					isDrinked = false;
				}
				else if (!isDrinked)
				{
					Vedenkeitin vesiKeitin = GetNode<Vedenkeitin>("../Vedenkeitin");
					vesiKeitin.TakasAlkuun();
					isDrinked = true;
				}
			}
		}
	}

	void onBodyEntered(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			GD.Print("Entered");
			GD.Print("Press F to open");
			areaEntered = true;

		}

	}

	void onBodyExited(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			GD.Print("Left the prox");
			areaEntered = false;
		}
	}
}
