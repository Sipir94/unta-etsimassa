extends ISceneLoader

class_name SceneLoader

var _path : String
var _loader : IResourceLoader
var _loading_thread : Thread
var _timer : Timer

func interactive_load(
		loader : ResourceInteractiveLoader) -> int:
	emit_signal("scene_load_started")
	
	var stage_count = loader.get_stage_count()
	
	while true:
		var status = loader.poll()
		if (status == OK):
			emit_signal("load_status", (loader.get_stage() * 100.0) / stage_count)
			continue
		elif (status == ERR_FILE_EOF):
			emit_signal("load_status", 100.0)
			scene_resource = loader.get_resource()
			_timer.start()
			return OK
		else:
			print("Error whie loadng level: " + str(status))
			break
	
	return FAILED

func _init(
		path : String,
		loader : IResourceLoader).(path, loader) -> void:
	_path = path
	
	if(loader == null):
		_loader = ResourceLoaderWrapper.new()
	else:
		_loader = loader
	
	_timer = Timer.new()
	_timer.wait_time = 0.5;
# warning-ignore:return_value_discarded
	_timer.connect("timeout", self, "loading_done")
	add_child(_timer)

func start_loading() -> void:
	_load_scene(_path)

func _load_scene(path : String) -> void:
	if (_loader.has_cached(path)):
		emit_signal("scene_load_started", _loader.load(path))
	else:
		var resource_interactive_loader = _loader.load_interactive(path)
		_loading_thread = Thread.new()
		var status = _loading_thread.start(
							self,
							"interactive_load",
							resource_interactive_loader)
		
		if (status == ERR_CANT_CREATE):
			print("Unable to create loading thread!\n Error: " + str(status))

func loading_done():
	_loading_thread.wait_to_finish()
	emit_signal("scene_load_ended")
