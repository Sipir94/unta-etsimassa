extends Node

class_name IResourceLoader

func _init():
	pass

func _ready():
	pass

func load(_path : String) -> Resource:
	return null
	
func load_interactive(_path : String) -> ResourceInteractiveLoader:
	return null

func has_cached(_path : String) -> bool:
	return false
