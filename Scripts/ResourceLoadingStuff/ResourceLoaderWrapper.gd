extends IResourceLoader

class_name ResourceLoaderWrapper

func load(path : String) -> Resource:
	return ResourceLoader.load(path)

func load_interactive(path : String) -> ResourceInteractiveLoader:
	return ResourceLoader.load_interactive(path)

func has_cached(path : String) -> bool:
	return ResourceLoader.has_cached(path)
