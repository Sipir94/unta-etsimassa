extends Node

class_name ISceneLoader

# warning-ignore:unused_signal
signal scene_load_started
# warning-ignore:unused_signal
signal load_status(precentage)
# warning-ignore:unused_signal
signal scene_load_ended

var scene_resource : Resource

func _init(_path : String, _loader : IResourceLoader) -> void:
	pass
	
func start_loading() -> void:
	pass
