extends Node

export (NodePath) var targetNodePath
onready var targetNode = get_node(targetNodePath) as Spatial

export (NodePath) var targetNodePath1
onready var targetNode1 = get_node(targetNodePath1) as Spatial

export (float) var speed = 0.4
var t = 0.0
var f = true;
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	t += delta * speed
	var p1 = targetNode.transform
	var p2 = targetNode1.transform
	
	if(f == true):
		self.transform = p1.interpolate_with(p2, t)
		if(t > 1):
			f = false
			t = 0.0
	else :
		var d = self.transform.origin.distance_to(p1.origin)
		self.transform = p2.interpolate_with(p1, t)
		if(t > 1):
			f = true
			t = 0.0
	#self.position = p1.linear_interpolate(p2, t)
