using Godot;
using System;

public class LightSwitch : Spatial
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private bool areaEntered = false;
	private bool lightsOn = false;

	Godot.Collections.Array valot = new Godot.Collections.Array();

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		foreach (Node item in GetChildren())
		{
			if (item.IsInGroup("valot"))
			{
				valot.Add(item);
			}
		}
	}

	public override void _Process(float delta)
	{
		if (Input.IsActionJustPressed("UniversalF"))
		{
			if (areaEntered)
			{
				if (!lightsOn)
				{
					foreach (OmniLight item in valot)
					{
						item.Visible = true;
					}
					lightsOn = true;
				}
				else if (lightsOn)
				{
					foreach (OmniLight item in valot)
					{
						item.Visible = false;
					}
					lightsOn = false;
				}
			}
		}
	}

	void onBodyEntered(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			GD.Print("Entered");
			GD.Print("Press F to open");
			areaEntered = true;

		}

	}

	void onBodyExited(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			GD.Print("Left the prox");
			areaEntered = false;
		}
	}
}
