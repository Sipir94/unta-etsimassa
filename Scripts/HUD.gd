extends Control

export(NodePath) var _interact_text_path
export(NodePath) var nukkumatti_count_text_path
export(NodePath) var nukkumatti_path

onready var nukkumatti = get_node(nukkumatti_path) as Nukkumatti
onready var _interact_text_node = get_node(_interact_text_path) as Control
onready var _nukkumatti_count_text = get_node(nukkumatti_count_text_path) as Label

var nukkumatti_count = 0

func _ready():
	nukkumatti.connect("found", self, "_on_nukkumatti_found")

func _process(_delta):
	# Just testing
	if(Input.is_key_pressed(KEY_H)):
		show_interact_text()
	elif(Input.is_key_pressed(KEY_J)):
		hide_interact_text()

func show_interact_text() -> void:
	_interact_text_node.show()
	
func hide_interact_text() -> void:
	_interact_text_node.hide()

func _on_nukkumatti_found():
	nukkumatti_count += 1
	_nukkumatti_count_text.text = "Nukkumatti found: " + str(nukkumatti_count) + "/5"
