extends Menu

class_name MainMenu

export(NodePath) var loading_screen_path : NodePath
export(String) var game_scene_path : String

onready var _loading_screen = get_node(loading_screen_path) as LoadingScreen
onready var _scene_loader = SceneLoader.new(game_scene_path, null) as ISceneLoader

func _ready():
	_scene_loader.connect("load_status", _loading_screen, "update_progress_bar")
	_scene_loader.connect("scene_load_ended", self, "_scene_loaded")
	add_child(_scene_loader)

# Exit game when quit is pressed
func _on_quit_pressed():
	get_tree().quit()

func _on_start_game_pressed():
	_loading_screen.show()
	_scene_loader.start_loading()
	
func _scene_loaded() -> void:
	var scene = _scene_loader.scene_resource.instance()
	
	_loading_screen.hide()
	
	for child in get_children():
		remove_child(child)
		child.queue_free()
	
	add_child(scene)
