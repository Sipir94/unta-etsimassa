extends Control

class_name PauseMenu

export(String) var _main_menu_path : String
export(NodePath) var _loading_screen_path : NodePath

onready var _scene_loader = SceneLoader.new(_main_menu_path, null) as ISceneLoader
onready var _loading_screen = get_node(_loading_screen_path) as LoadingScreen
onready var _main = get_tree().get_root().get_node("Main")
var _continue_button : Button


func _ready():
	_scene_loader.connect("load_status", _loading_screen, "update_progress_bar")
	_scene_loader.connect("scene_load_ended", self, "_scene_loading_ended")
	_main.add_child(_scene_loader)
	
	_continue_button = get_node("VBoxContainer/Continue")

func _process(_delta):
	if(Input.is_key_pressed(KEY_ESCAPE)):
		self.show()

func _on_Continue_pressed():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	self.hide()

func _on_Quit_to_main_menu_pressed():
	self.hide()
	_loading_screen.show()
	_scene_loader.start_loading()

func _scene_loading_ended() -> void:
	var scene = _scene_loader.scene_resource.instance()
	
	_loading_screen.hide()
	
	var childrens = _main.get_children()
	childrens.remove(childrens.find(_loading_screen))
	
	for child in childrens:
		_main.remove_child(child)
		child.queue_free()
	
	_main.add_child(scene)


func _on_lose():
	_continue_button.hide()
	self.show()
