extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (NodePath) var targetNodePath
onready var targetNode = get_node(targetNodePath) as Spatial
var targetPos = null
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	targetPos = targetNode.transform.origin
	self.look_at(targetPos, Vector3(0,1,0))
