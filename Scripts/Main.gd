extends Node

onready var scene_loader = SceneLoader.new("res://Scenes/Menu.tscn", null) as ISceneLoader
onready var _loading_screen = get_node("Loading Screen") as LoadingScreen

func _ready():
	OS.window_fullscreen = false
	
# warning-ignore:return_value_discarded
	scene_loader.connect("scene_load_started", self, "_scene_loading_started")
# warning-ignore:return_value_discarded
	scene_loader.connect("scene_load_ended", self, "_scene_loading_ended")
	scene_loader.connect("load_status", _loading_screen, "update_progress_bar")
	add_child(scene_loader)
	scene_loader.start_loading()
	
func _scene_loading_started():
	print("Started loading scene")
	
func _scene_loading_ended():
	print("Ended loading scene")
	var scene = scene_loader.scene_resource.instance()
	
	_loading_screen.hide()
	
	for child in get_children():
		remove_child(child)
		child.queue_free()
	
	add_child(scene)
