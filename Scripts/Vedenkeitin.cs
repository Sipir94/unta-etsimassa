using Godot;
using System;

public class Vedenkeitin : Spatial
{
    bool areaEntered = false;

    KinematicBody teekuppi;
    MeshInstance runko;
    Tween tweenMovement;
    OmniLight valo;

    bool isHeated = false;

    Vector3 runkoOrig;
    Vector3 runkoDeg;

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        teekuppi = GetNode<KinematicBody>("../Teekuppi");
        runko = GetNode<MeshInstance>("Runko");
        tweenMovement = GetNode<Tween>("Tweener");
        valo = GetNode<OmniLight>("Runko/MeshInstance/OmniLight");

        runkoOrig = runko.Transform.origin;
        runkoDeg = runko.RotationDegrees;
    }

    // Called every frame. 'delta' is the elapsed time since the previous frame.
    public override void _Process(float delta)
    {
        if (Input.IsActionJustPressed("UniversalF"))
        {
            if (areaEntered)
            {
                if (!isHeated)
                {
                    valo.Visible = true;
                    HeatingTimer();
                }
                else
                {
                    tweenMovement.InterpolateProperty(runko,"translation",
                    runko.Transform.origin,GetNode<Spatial>("PourPos").Transform.origin,1,
                    Tween.TransitionType.Linear,Tween.EaseType.InOut,0);
                    tweenMovement.Start();

                    tweenMovement.InterpolateProperty(runko,"rotation_degrees",
                    runko.RotationDegrees,GetNode<Spatial>("PourPos").RotationDegrees,1,
                    Tween.TransitionType.Linear,Tween.EaseType.InOut,0);
                    tweenMovement.Start();

                    Teekuppi realteekuppi = (Teekuppi)teekuppi;
                    realteekuppi.isReady = true;
                }
                
                // tweenMovement.InterpolateProperty(runko,"translation",
                // runko.Transform.origin,runkoOrig,1,
                // Tween.TransitionType.Linear,Tween.EaseType.InOut,0);
                // tweenMovement.Start();

                // tweenMovement.InterpolateProperty(runko,"rotation_degrees",
                // runko.RotationDegrees,runkoDeg,1,
                // Tween.TransitionType.Linear,Tween.EaseType.InOut,0);
                // tweenMovement.Start();
            }
        }
    }

    private async void HeatingTimer()
    {
        await ToSignal(GetTree().CreateTimer(5),"timeout");
        valo.Visible = false;
        isHeated = true;
    }

    void onBodyEntered(KinematicBody body)
    {
        if (body.Name == "Player")
        {
            GD.Print("Entered");
            GD.Print("Press F to open");
            areaEntered = true;

        }

    }

    void onBodyExited(KinematicBody body)
    {
        if (body.Name == "Player")
        {
            GD.Print("Left the prox");
            areaEntered = false;
        }
    }

    public void TakasAlkuun()
    {
        tweenMovement.InterpolateProperty(runko,"translation",
        runko.Transform.origin,runkoOrig,1,
        Tween.TransitionType.Linear,Tween.EaseType.InOut,0);
        tweenMovement.Start();

        tweenMovement.InterpolateProperty(runko,"rotation_degrees",
        runko.RotationDegrees,runkoDeg,1,
        Tween.TransitionType.Linear,Tween.EaseType.InOut,0);
        tweenMovement.Start();
    }
}
