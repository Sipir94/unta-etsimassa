using Godot;
using System;

public class Door : Area
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";

	private bool areaEntered = false;
	private bool doorOpen = false;

	Spatial pivot;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		pivot = GetNode<Spatial>("../Spatial");
	}

	public override void _Process(float delta)
	{
		if (Input.IsActionJustPressed("UniversalF"))
		{
			if (areaEntered)
			{
				if (!doorOpen)
				{
					pivot.RotateY(Mathf.Deg2Rad(-90));
					doorOpen = true;
				}
				else if (doorOpen)
				{
					pivot.RotateY(Mathf.Deg2Rad(90));
					doorOpen = false;
				}
			}
		}
	}

	void onBodyEntered(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			GD.Print("Entered");
			GD.Print("Press F to open");
			areaEntered = true;

		}

	}

	void onBodyExited(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			GD.Print("Left the prox");
			areaEntered = false;
		}
	}
	
}
