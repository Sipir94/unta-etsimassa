extends KinematicBody

class_name NukkumattiPhysics

signal player_entered_area
signal player_exited_area

func _on_body_entered(body) -> void:
	if(body.name == "Player"):
		emit_signal("player_entered_area")
		

func _on_body_exited(body) -> void:
	if(body.name == "Player"):
		emit_signal("player_exited_area")
