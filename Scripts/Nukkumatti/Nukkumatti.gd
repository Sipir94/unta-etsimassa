extends IInteractable

class_name Nukkumatti

signal found

export(Array) var spawn_positions_paths

var _spawn_transforms : Array
var _rng = RandomNumberGenerator.new()

onready var _can_grab_nukkumatti = false
onready var _nukkumatti = get_child(0) as NukkumattiPhysics

func interact() -> void:
	emit_signal("found")
	self.hide()
	_update_location()
	self.show()

func _ready() -> void:
	for child in spawn_positions_paths:
		var node = get_node(child) as Spatial
		_spawn_transforms.append(node.transform)
	
	_update_location()
	
func _process(_delta):	
	if(_can_grab_nukkumatti && Input.is_action_just_pressed("UniversalE")):
		interact()
		_can_grab_nukkumatti = false
	
func _update_location() -> void:
	_rng.randomize()
	var random_location_index = _rng.randi_range(0, _spawn_transforms.size() - 1)
	self.transform = _spawn_transforms[random_location_index]

func _on_player_entered_area() -> void:
	_can_grab_nukkumatti = true
	
func _on_NukkumattiModel_player_exited_area():
	_can_grab_nukkumatti = false
