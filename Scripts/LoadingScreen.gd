extends Control

class_name LoadingScreen

export(NodePath) var progress_bar_path
onready var _progress_bar = get_node(progress_bar_path) as ProgressBar

	
func update_progress_bar(precentage : float):
	_progress_bar.value = precentage
