extends Spatial

# Declare member variables here. Examples:
export (NodePath) var hours_needle_path
onready var hours_needle = get_node(hours_needle_path)

export (NodePath) var minute_needle_path
onready var minute_needle = get_node(minute_needle_path)

export (NodePath) var seconds_needle_path
onready var seconds_needle = get_node(seconds_needle_path)

var radian_seconds = null
var radian_minutes = null
var radian_hours = null

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var h = OS.get_time()["hour"]
	var m = OS.get_time()["minute"]
	var s = OS.get_time()["second"]
	
	radian_seconds = OS.get_time()["second"] * (PI/30)
	radian_minutes = OS.get_time()["minute"] * (PI/30)
	radian_hours = OS.get_time()["hour"] * (PI/6)
	
	print("H ", h, " M ", m , " S ", s)
	
	var rot = hours_needle.get("rotation")
	rot.z = -radian_hours
	hours_needle.set("rotation", rot)
	
	rot = minute_needle.get("rotation")
	rot.z = -radian_minutes
	minute_needle.set("rotation", rot)
	
	rot = seconds_needle.get("rotation")
	rot.z = -radian_seconds
	seconds_needle.set("rotation", rot)
