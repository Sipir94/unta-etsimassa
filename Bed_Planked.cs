using Godot;
using System;

public class Bed_Planked : Spatial
{
	private bool areaEntered = false;
	private bool canCrouch = true;
	KinematicBody player;
	Spatial crouchHeight;
	Camera pCamera;
	Vector3 pCameraOrig;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		
		
	}

	public override void _Process(float delta)
	{
		if (Input.IsActionJustPressed("UniversalF"))
		{
			if (areaEntered)
			{
				if (canCrouch)
				{
					crouchHeight = player.GetNode<Spatial>("CrouchSpot");
					pCamera = player.GetNode<Camera>("Camera");
					pCameraOrig = pCamera.Translation;
					pCamera.Translate(crouchHeight.Translation);
					//pCamera.Translation = crouchHeight.Translation;
					canCrouch = false;
				}
				else if (!canCrouch)
				{
					pCamera.Translation = pCameraOrig;
					canCrouch = true;
				}
			}
		}
	}

	void onBodyEntered(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			player = body;
			GD.Print("Entered");
			GD.Print("Press F to open");
			areaEntered = true;

		}

	}

	void onBodyExited(KinematicBody body)
	{
		if (body.Name == "Player")
		{
			if (!canCrouch)
			{
				pCamera.Translation = pCameraOrig;
				canCrouch = true;
			}
			GD.Print("Left the prox");
			areaEntered = false;
		}
	}
}
