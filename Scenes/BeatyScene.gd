extends Spatial

signal lose

export(NodePath) var clocks_path
onready var clocks_parent = get_node(clocks_path) as Spatial

var _clocks : Array

# Called when the node enters the scene tree for the first time.
func _ready():
	_clocks.resize(clocks_parent.get_children().size())
	
	for clock in clocks_parent.get_children():
		clock.connect("lose", self, "_lose")
	

func _lose():
	emit_signal("lose")
